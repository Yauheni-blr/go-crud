package main

import (
	"log"
	"net/http"
)

// TODO:
// 1. get all ../users
// 2. get one ../users/:id
// 3. set one ../users
// 5. delete one ../users/:id

func main() {
	http.HandleFunc("/users/", router)
	log.Fatal(http.ListenAndServe(":8080", nil))
}
