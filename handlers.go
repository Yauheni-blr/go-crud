package main

import (
	"encoding/json"
	"net/http"
	"strconv"
	"strings"
	"time"
)

// User - user struct
type User struct {
	ID   string `json:"id"`
	Name string `json:"name"`
	Age  int    `json:"age"`
}

var store = map[string]User{
	"afsd657": {
		ID:   "afsd657",
		Name: "Joch",
		Age:  25,
	},
}

func getID(path string) string {
	parts := strings.Split(path, "/")
	if len(parts) > 2 && parts[2] == "" {
		return ""
	}
	return parts[2]
}

func getUsers(w http.ResponseWriter, req *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	err := json.NewEncoder(w).Encode(store)
	if err != nil {
		http.Error(w, "something goes wrong", http.StatusInternalServerError)
		return
	}
}

func getUser(w http.ResponseWriter, id string) {
	user, found := store[id]
	if !found {
		http.Error(w, "user not found", http.StatusNotFound)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	err := json.NewEncoder(w).Encode(user)
	if err != nil {
		http.Error(w, "something goes wrong", http.StatusInternalServerError)
		return
	}
}

func createUser(w http.ResponseWriter, req *http.Request) {
	if req.Body == nil {
		http.Error(w, "body required", http.StatusBadRequest)
		return
	}

	user := User{}
	err := json.NewDecoder(req.Body).Decode(&user)
	if err != nil {
		panic(err)
	}

	user.ID = strconv.FormatInt(time.Now().UnixNano()/1000000, 10)

	userJSON, err := json.Marshal(user)
	if err != nil {
		panic(err)
	}

	store[user.ID] = user

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusCreated)
	w.Write(userJSON)
}

func deleteUser(w http.ResponseWriter, id string) {
	_, found := store[id]
	if !found {
		http.Error(w, "user not found", http.StatusNotFound)
		return
	}

	delete(store, id)

	w.WriteHeader(http.StatusNoContent)
}

func router(w http.ResponseWriter, req *http.Request) {
	id := getID(req.URL.Path)

	switch req.Method {

	case http.MethodGet:
		if id != "" {
			getUser(w, id)
		} else {
			getUsers(w, req)
		}
		return

	case http.MethodPost:
		createUser(w, req)
		return

	case http.MethodDelete:
		deleteUser(w, id)

	default:
		http.Error(w, "unsupported method", http.StatusUnsupportedMediaType)
		return

	}
}
